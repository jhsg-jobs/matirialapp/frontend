import {FlatTreeControl} from '@angular/cdk/tree';
import { Component, OnInit } from '@angular/core';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import {MatDialog} from '@angular/material/dialog';
import { ModalCadMaterialComponent } from './components/modal-cad-material/modal-cad-material.component';

/**
 * Food data with nested structure.
 * Each node has a name and an optional list of children.
 */
interface FoodNode {
  name: string;
  children?: FoodNode[];
}

const TREE_DATA: FoodNode[] = [
  {
    name: 'Moleton',
    children: [
      {name: 'Botão'},
      {name: 'Linha Preta'},
      {
        name: 'Tecido Preto',
        children: [
          {name: 'Corante Preto'},
          {name: 'Algodão Puro'},
        ]
      },
    ]
  }, {
    name: 'Calça Jeans',
    children: [
      {name: 'Botão'},
      {name: 'Linha Preta'},
      {
        name: 'Tecido Jeans',
        children: [
          {name: 'Corante Azul'},
          {name: 'Algodão Puro'},
        ]
      },
      {name: 'Zipper'}
    ]
  },
];

/** Flat node with expandable and level information */
interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
}

@Component({
  selector: 'app-materiais',
  templateUrl: './materiais.component.html',
  styleUrls: ['./materiais.component.css']
})
export class MateriaisComponent implements OnInit {


  constructor(public dialog: MatDialog) {
    this.dataSource.data = TREE_DATA;
  }

  ngOnInit(): void {
  }

  private _transformer = (node: FoodNode, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      level: level,
    };
  }

  treeControl = new FlatTreeControl<ExampleFlatNode>(
      node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
      this._transformer, node => node.level, node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);


  hasChild = (_: number, node: ExampleFlatNode) => node.expandable;

  openDialog() {
    this.dialog.open(ModalCadMaterialComponent);
  }

}

