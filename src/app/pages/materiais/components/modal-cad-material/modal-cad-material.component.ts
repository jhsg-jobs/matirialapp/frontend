import { Component, OnInit } from '@angular/core';
import {ThemePalette} from '@angular/material/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-modal-cad-material',
  templateUrl: './modal-cad-material.component.html',
  styleUrls: ['./modal-cad-material.component.css']
})
export class ModalCadMaterialComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  toppingList: string[] = ['Botão', 'Linha Preta', 'Tecido Jeans', 'Corante Azul', 'Algodão Puro', 'Zipper'];

  color: ThemePalette = 'accent';
  checked = false;
  disabled = false;

}
