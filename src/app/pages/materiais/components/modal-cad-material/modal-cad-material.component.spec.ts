import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCadMaterialComponent } from './modal-cad-material.component';

describe('ModalCadMaterialComponent', () => {
  let component: ModalCadMaterialComponent;
  let fixture: ComponentFixture<ModalCadMaterialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCadMaterialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCadMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
