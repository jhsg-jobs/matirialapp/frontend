import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { MateriaisComponent } from './pages/materiais/materiais.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'materiais',
    component: MateriaisComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
